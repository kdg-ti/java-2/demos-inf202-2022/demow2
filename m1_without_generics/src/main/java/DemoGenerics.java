import java.util.ArrayList;
import java.util.List;

/**
 * Sebastiaan Aussems
 * 20/09/2022
 */
public class DemoGenerics {
    public static void main(String[] args) {
        List myList = new ArrayList();
        myList.add("test");
        myList.add(4);
        System.out.println(myList);

    }
}
