import model.magic_cards.CreatureCard;
import model.magic_cards.LandCard;
import model.magic_cards.MagicCard;
import model.magic_cards.SpellCard;
import model.playing_cards.PlayingCard;

import java.util.ArrayList;
import java.util.List;

/**
 * Demo voor het gebruik van wildcards in combinatie met generics
 *
 * Sebastiaan Aussems
 * 20/09/2022
 */
public class DemoWildcards {
    public static void main(String[] args) {
        List<PlayingCard> myPlayingCards = new ArrayList<>();
        String[] suits = {"Hearts", "Diamonds", "Clubs", "Spades"};
        String[] ranks = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace"};

        for (String suit : suits) {
            for (String rank : ranks) {
                myPlayingCards.add(new PlayingCard(rank + "of " + suit,suit,rank));
            }
        }

        // Create an ArrayList to store different types of MagicCards
        ArrayList<MagicCard> deck = new ArrayList<>();

        // Add different types of MagicCards to the deck
        deck.add(new CreatureCard("Serra Angel", "White", 5, 4, 4));
        deck.add(new SpellCard("Lightning Bolt", "Red", 1, "Deal 3 damage to any target"));
        deck.add(new LandCard("Forest", "Basic"));
        deck.add(new CreatureCard("Shivan Dragon", "Red", 6, 5, 5));
        deck.add(new SpellCard("Counterspell", "Blue", 2, "Counter target spell"));
        deck.add(new LandCard("Island", "Basic"));

        // Create an ArrayList to store CreatureCards
        ArrayList<CreatureCard> creatureDeck = new ArrayList<>();

        // Add CreatureCard objects to the creatureDeck
        creatureDeck.add(new CreatureCard("Serra Angel", "White", 5, 4, 4));
        creatureDeck.add(new CreatureCard("Shivan Dragon", "Red", 6, 5, 5));
        creatureDeck.add(new CreatureCard("Llanowar Elves", "Green", 1, 1, 1));
        creatureDeck.add(new CreatureCard("Sengir Vampire", "Black", 5, 4, 4));
        creatureDeck.add(new CreatureCard("Grizzly Bears", "Green", 2, 2, 2));

        System.out.println("OK");
    }
}
