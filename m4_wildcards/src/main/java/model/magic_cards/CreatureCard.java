package model.magic_cards;

public class CreatureCard extends MagicCard {
    private int power;
    private int toughness;

    public CreatureCard(String name, String color, int manaCost, int power, int toughness) {
        super(name, color, manaCost);
        this.power = power;
        this.toughness = toughness;
    }

    public int getPower() {
        return power;
    }

    public int getToughness() {
        return toughness;
    }

    @Override
    public String play() {
        return getName() + " (Creature) enters the battlefield with power " + power + " and toughness " + toughness;
    }

    @Override
    public String toString() {
        return super.toString() + " [Creature] (Power: " + power + ", Toughness: " + toughness + ")";
    }
}
