package model.magic_cards;

public class LandCard extends MagicCard {
    private String landType;

    public LandCard(String name, String landType) {
        super(name, "Colorless", 0); // Land cards generally don't have a mana cost
        this.landType = landType;
    }

    public String getLandType() {
        return landType;
    }

    @Override
    public String play() {
        return getName() + " (Land) is played. It is a " + landType + " land.";
    }

    @Override
    public String toString() {
        return super.toString() + " [Land] (Type: " + landType + ")";
    }
}