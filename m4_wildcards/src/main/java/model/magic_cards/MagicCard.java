package model.magic_cards;

import model.Card;

/**
 * Sebastiaan Aussems
 * 20/09/2022
 */
public abstract class MagicCard extends Card {
    private String color;
    private int manaCost;

    public MagicCard(String name, String color, int manaCost) {
        super(name);
        this.color = color;
        this.manaCost = manaCost;
    }

    // Getters
    public String getColor() {
        return color;
    }

    public int getManaCost() {
        return manaCost;
    }

    // Abstract method for card's effect (to be implemented in subclasses)
    public abstract String play();

    @Override
    public String toString() {
        return getName() + " - " + color + " (Mana Cost: " + manaCost + ")";
    }
}
