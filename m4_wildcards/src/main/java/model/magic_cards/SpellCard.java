package model.magic_cards;

public class SpellCard extends MagicCard {
    private String effect;

    public SpellCard(String name, String color, int manaCost, String effect) {
        super(name, color, manaCost);
        this.effect = effect;
    }

    public String getEffect() {
        return effect;
    }

    @Override
    public String play() {
        return getName() + " (Spell) is cast. Effect: " + effect;
    }

    @Override
    public String toString() {
        return super.toString() + " [Spell] (Effect: " + effect + ")";
    }
}
