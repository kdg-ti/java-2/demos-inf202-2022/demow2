package model.playing_cards;

import model.Card;

/**
 * Sebastiaan Aussems
 * 20/09/2022
 */
public class PlayingCard extends Card {
    private String suit;
    private String rank;

    public PlayingCard(String name, String suit, String rank) {
        super(name);
        this.suit = suit;
        this.rank = rank;
    }

    public String getSuit() {
        return suit;
    }

    public String getRank() {
        return rank;
    }

    @Override
    public String play() {
        return suit + " " + rank + " is played!";
    }
}
